﻿using Application.Service.Interface;
using Domain.Entities;
using OffShore.Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Service.Implementations
{
    public class EmployeeService : IService<Employee>
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Task<Employee> Add(Employee entity)
        {
            try
            {
                return _employeeRepository.Add(entity);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<Employee> Delete(int id)
        {
            try
            {
                return _employeeRepository.Delete(id);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<IEnumerable<Employee>> GetAll()
        {
            try
            {
                return _employeeRepository.GetAll();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<Employee> GetById(int id)
        {
            try
            {
                return _employeeRepository.GetById(id);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<Employee> Update(int id, Employee entity)
        {
            try
            {
                return _employeeRepository.Update(id, entity);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }
    }
}
