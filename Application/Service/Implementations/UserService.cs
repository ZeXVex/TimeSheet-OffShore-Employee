﻿using Application.Service.Interface;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Service.Implementations
{
    public class UserService : IUserService<User>
    {
        private readonly IUserService<User> _userService;

        public UserService(IUserService<User> userService)
        {
            _userService = userService;
        }

        public Task<User> Add(User entity)
        {
            try
            {
                return _userService.Add(entity);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task Authenticate(object username, object password)
        {
            try
            {
                return _userService.Authenticate(username, password);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<IEnumerable<User>> GetAll()
        {
            try
            {
                return _userService.GetAll();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public Task<User> GetById(int id)
        {
            try
            {
                return (_userService.GetById(id));
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }
    }
}
