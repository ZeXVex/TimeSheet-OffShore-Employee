﻿using Application.Service.Implementations;
using Application.Service.Interface;
using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services)
        {
            // Dependency injection for employee
            services.AddScoped<IService<Employee>, EmployeeService>();

            // Dependency injection for user
            services.AddScoped<IUserService<User>, UserService>();
        }
    }
}
