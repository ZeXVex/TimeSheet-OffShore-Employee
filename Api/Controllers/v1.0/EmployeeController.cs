﻿using Application.Service.Interface;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers.v1._0
{
    [Authorize]
    [ApiVersion("1.0")]
    public class EmployeeController : BaseApiController
    {
        private readonly IService<Employee> _employeeService;

        public EmployeeController(IService<Employee> employeeService)
        {
            _employeeService = employeeService;
        }

        [Authorize("Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(Employee entity)
        {
            try
            {
                await _employeeService.Add(entity);
                return Ok();
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<Employee> listOfAllEmployee = await _employeeService.GetAll();
                return Ok(listOfAllEmployee);
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Employee selectEmployee = await _employeeService.GetById(id);
                if (selectEmployee == null)
                {
                    return NotFound("Given employee could not be found");
                }
                return Ok(selectEmployee);
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Employee deleteEmployee = await _employeeService.Delete(id);

                if (deleteEmployee == null)
                {
                    return NotFound();
                }

                return Ok(null);
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Employee entity)
        {
            try
            {
                if (id != entity.EmployeeId)
                {
                    return NotFound("Given employee could not be found");
                }

                Employee updateEmployee = await _employeeService.Update(id, entity);

                if (updateEmployee == null)
                {
                    return BadRequest("Given employee could not be updated");
                }

                return Ok(updateEmployee);
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }
    }
}
