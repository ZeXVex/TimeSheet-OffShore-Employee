﻿using Application.Service.Interface;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Api.Controllers.v1._0
{
    [Authorize]
    [ApiVersion("1.0")]
    public class UsersController : BaseApiController
    {
        private IUserService<User> _userService;

        public UsersController(IUserService<User> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userService.GetAll();
            return Ok(users);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Create(User entity)
        {
            try
            {
                await _userService.Add(entity);
                return Ok();
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login([FromBody] User user)
        {
            var token = _userService.Authenticate(user.Email, user.Password);

            if (token == null)
                return Unauthorized();

            return Ok(token);
        }
    }
}
