﻿using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;
using OffShore.Infrastructure.Context;
using OffShore.Infrastructure.Repositories.Implementation;
using OffShore.Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddPersistance(this IServiceCollection service)
        {
            // Dependency inject for employee repository
            service.AddScoped<IRepository<Employee>, EmployeeRepository>();

            // Dependency inject for user repository
            service.AddScoped<IUserService<User>, UserRepository>();

            // Dependency injection for service
            service.AddSingleton<ApplicationContext>();
        }
    }
}
