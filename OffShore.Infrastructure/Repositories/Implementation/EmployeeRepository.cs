﻿using Domain;
using Domain.Entities;
using MongoDB.Driver;
using OffShore.Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Repositories.Implementation
{
    public class EmployeeRepository : IRepository<Employee>
    {
        public readonly IApplicationContext _context;

        public EmployeeRepository(IApplicationContext context)
        {
            _context = context;
        }

        public async Task<Employee> Add(Employee entity)
        {
            try
            {
                await _context.Employee.InsertOneAsync(entity);
                return entity;
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<Employee> Delete(int id)
        {
            try
            {
                return await _context.Employee.FindOneAndDeleteAsync(e => e.EmployeeId == id);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            try
            {
                return await _context.Employee.Find(_ => true).ToListAsync();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<Employee> GetById(int id)
        {
            try
            {
                return await _context.Employee.Find(e => e.EmployeeId == id).FirstAsync();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<Employee> Update(int id, Employee entity)
        {
            try
            {
                await _context.Employee.ReplaceOneAsync(e => e.EmployeeId == id, entity);
                return entity;
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }
    }
}
