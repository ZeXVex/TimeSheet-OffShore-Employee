﻿using Domain;
using Domain.Entities;
using MongoDB.Driver;
using OffShore.Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Repositories.Implementation
{
    public class TimesheetRepository : ITimesheetRepository<TimeSheet>
    {
        public readonly IApplicationContext _context;

        public TimesheetRepository(IApplicationContext context)
        {
            _context = context;
        }

        /*public async Task<TimeSheet> Add(TimeSheet entity)
        {
            try
            {
                await _context.TimeSheet.InsertOneAsync(entity);
                return entity;
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<IEnumerable<TimeSheet>> GetAll()
        {
            try
            {
                return await _context.TimeSheet.Find(_ => true).ToListAsync();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<TimeSheet> GetById(int id)
        {
            try
            {
                return await _context.TimeSheet.Find(e => e.TimeSheetId == id).FirstAsync();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }*/
    }
}
