﻿using Domain;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using OffShore.Infrastructure.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Repositories.Implementation
{
    public class UserRepository : IUserService<User>
    {
        public readonly IApplicationContext _context;
        private readonly string key;

        public UserRepository(IApplicationContext context)
        {
            _context = context;
        }


        public async Task<User> Add(User entity)
        {
            try
            {
                await _context.User.InsertOneAsync(entity);
                return entity;
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public string Authenticate(User entity)
        {
            try
            {
            var user = this._context.User.Find(x => x.Email == entity.Email && x.Password == entity.Password);

            if (user == null)
                return null;

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.ASCII.GetBytes(key);
            var toeknDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, entity.Email),
                }),

                Expires = DateTime.UtcNow.AddHours(1),

                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(tokenKey),
                    SecurityAlgorithms.HmacSha256Signature
                    )
            };
            var token = tokenHandler.CreateToken(toeknDescriptor);

            return tokenHandler.WriteToken(token);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            try
            {
                return (IEnumerable<User>)await _context.Employee.Find(_ => true).ToListAsync();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message, x.InnerException);
            }
        }
    }
}
