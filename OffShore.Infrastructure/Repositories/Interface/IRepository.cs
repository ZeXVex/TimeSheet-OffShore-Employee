﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Repositories.Interface
{
    public interface IRepository<T>
    {
        Task<T> Add(T entity);
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<T> Update(int id, T entity);
        Task<T> Delete(int id);
    }
}
