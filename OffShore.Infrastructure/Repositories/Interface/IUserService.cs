﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Repositories.Interface
{
    public interface IUserService<T>
    {
        Task<T> Add(T entity);
        string Authenticate(T entity);
        Task<IEnumerable<T>> GetAll();
    }
}
