﻿using Domain;
using Domain.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OffShore.Infrastructure.Context
{
    public class ApplicationContext : IApplicationContext
    {
        public ApplicationContext(IMongoDBSettings mongoSettings)
        {
            MongoClient client = new MongoClient(mongoSettings.ConnectionString);
            IMongoDatabase _database = client.GetDatabase(mongoSettings.DatabaseName);

            Employee = _database.GetCollection<Employee>(mongoSettings.Employee);
            User = _database.GetCollection<User>(mongoSettings.User);
            //TimeSheet = _database.GetCollection<TimeSheet>(mongoSettings.TimeSheet);
        }

        public IMongoCollection<Employee> Employee { get; set; }
        //public IMongoCollection<TimeSheet> TimeSheet { get; set; }
        public IMongoCollection<User> User { get; set; }
    }
}
