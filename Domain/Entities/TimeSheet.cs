﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TimeSheet
    {
        public int TimeSheetId { get; set; }
        public Dictionary<string, DateTime> Travle { get; set; }
        public Dictionary<DateTime, double> TimeSheets { get; set; }
        //public DateTime Day { get; set; }
        //public double HoursWorked { get; set; }
    }
}
