﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Modles
{
    public class MongoDBSettings : IMongoDBSettings
    {
        public string User { get; set; }
        //public string TimeSheet { get; set; }
        public string Employee { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
