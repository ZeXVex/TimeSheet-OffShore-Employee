﻿using Domain.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IApplicationContext
    {
        IMongoCollection<Employee> Employee { get; set; }
        IMongoCollection<User> User { get; set; }
        //IMongoCollection<TimeSheet> TimeSheet { get; set; }
    }
}
