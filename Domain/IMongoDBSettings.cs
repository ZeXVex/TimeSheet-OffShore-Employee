﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IMongoDBSettings
    {
        string Employee { get; set; }
        string User { get; set; }
        //string TimeSheet { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
